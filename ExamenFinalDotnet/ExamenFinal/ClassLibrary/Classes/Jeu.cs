﻿using ClassLibrary.Classes.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Classes
{
	public class Jeu
	{
		private int id;
		private string nomJeu;
		private string plateforme;

		public int Id { get => id; set => id = value; }
		public string NomJeu { get => nomJeu; set => nomJeu = value; }
		public string Plateforme { get => plateforme; set => plateforme = value; }

		public Jeu(string nom_jeu, string nom_plateform)
		{
			this.NomJeu = nom_jeu;
			this.Plateforme = nom_plateform;
		}

		public Jeu() { }

		public string GenererNomJeu()
		{
			Random random = new Random();
			int nb = random.Next(100, 999);
			string result = nb.ToString();
			return $"Jeu{result}";
		}

		public string GenererNomPlateforme()
		{
			Random random = new Random();
			int nb = random.Next(100, 999);
			string result = nb.ToString();
			return $"Plateforme{result}";
		}
	}
}
