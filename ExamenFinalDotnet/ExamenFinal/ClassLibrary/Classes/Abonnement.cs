﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Classes.Personnes;

namespace ClassLibrary.Classes
{
	public class Abonnement
	{
		private int id;
		private Streamer streamer;
		private string nomAbonnement;
		private int dureeTotaleAbonnement;
		private bool estEnCours;


		public int Id { get => id; set => id = value; }
		public Streamer Streamer { get => streamer; set => streamer = value; }
		public string NomAbonnement { get => nomAbonnement; set => nomAbonnement = value; }
		public int DureeTotaleAbonnement { get => dureeTotaleAbonnement; set => dureeTotaleAbonnement = value; }
		public bool EstEnCours { get => estEnCours; set => estEnCours = value; }

		public Abonnement(Streamer streamer_, string nom_abonnement, int duree_totale_abonnement)
		{
			this.Streamer = streamer_;
			this.NomAbonnement = nom_abonnement;
			this.DureeTotaleAbonnement = duree_totale_abonnement;
			this.EstEnCours = false;
		}
	}
}
