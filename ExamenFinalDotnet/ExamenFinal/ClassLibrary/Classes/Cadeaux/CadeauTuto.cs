﻿using ClassLibrary.Classes.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Classes.Cadeaux
{
	public class CadeauTuto : Cadeau
	{
		private string nomDuJeu;
		private string plateForme;
		public string NomDuJeu { get => nomDuJeu; set => nomDuJeu = value; }
		public string PlateForme { get => plateForme; set => plateForme = value; }

		public CadeauTuto(string nom_cadeau, Streamer proprietaire, string type_cadeau, string nom_jeu, string plateforme)
		{
			this.NomCadeau = nom_cadeau;
			this.Proprietaire = proprietaire;
			this.TypeCadeau = type_cadeau;
			this.NomDuJeu = nom_jeu;
			this.PlateForme = plateforme;
		}

		public override string RetournerInfosCadeau()
		{
			return $"\n{this.NomCadeau} - ({this.TypeCadeau}) de {this.Proprietaire.Name}\nUn guide pour {this.NomDuJeu} sur {this.PlateForme}\n";
		}
	}
}
