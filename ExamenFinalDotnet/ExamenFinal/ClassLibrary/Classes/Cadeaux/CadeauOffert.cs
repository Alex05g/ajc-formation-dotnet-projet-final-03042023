﻿using ClassLibrary.Classes.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Classes.Cadeaux
{
	public class CadeauOffert : Cadeau
	{
		private string nomJeu;
		private string plateforme;
		public string NomJeu { get => nomJeu; set => nomJeu = value; }
		public string Plateforme { get => plateforme; set => plateforme = value; }

		public CadeauOffert(string nom_cadeau, Streamer proprietaire, string type_cadeau, string nom_jeu, string plateforme)
		{
			this.NomCadeau = nom_cadeau;
			this.Proprietaire = proprietaire;
			this.TypeCadeau = type_cadeau;
			this.NomJeu = nom_jeu;
			this.Plateforme = plateforme;
		}

		public override string RetournerInfosCadeau()
		{
			return $"\n{this.NomCadeau} - ({this.TypeCadeau}) de {this.Proprietaire.Name}\nLe jeu {NomJeu} sur {Plateforme}\n";
		}
	}
}
