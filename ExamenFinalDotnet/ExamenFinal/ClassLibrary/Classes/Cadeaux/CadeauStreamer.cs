﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Classes.Personnes;

namespace ClassLibrary.Classes.Cadeaux
{
	public class CadeauStreamer : Cadeau
	{
		private Streamer streamer;
		private int dureeJeu;
		public Streamer Streamer { get => streamer; set => streamer = value; }
		public int DureeJeu { get => dureeJeu; set => dureeJeu = value; }

		public CadeauStreamer(string nom_cadeau, Streamer proprietaire, string type_cadeau, Streamer streamer_, int duree_jeu)
		{
			this.NomCadeau = nom_cadeau;
			this.Proprietaire = proprietaire;
			this.TypeCadeau = type_cadeau;
			this.streamer = streamer_;
			this.DureeJeu = duree_jeu;
		}

		public override string RetournerInfosCadeau()
		{
			return $"\n{this.NomCadeau} - ({this.TypeCadeau}) de {this.Proprietaire.Name}\nUne session de jeu de {this.DureeJeu} heures avec {this.Streamer.Name}\n";
		}
	}
}
