﻿using ClassLibrary.Classes.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Classes.Cadeaux
{
	public class Cadeau
	{
		private int id;
		private string nomCadeau;
		private Streamer proprietaire;
		private string typeCadeau;
		public int Id { get => id; set => id = value; }
		public string NomCadeau { get => nomCadeau; set => nomCadeau = value; }
		public Streamer Proprietaire { get => proprietaire; set => proprietaire = value; }
		public string TypeCadeau { get => typeCadeau; set => typeCadeau = value; }

		public virtual string RetournerInfosCadeau()
		{
			return $"\n{this.NomCadeau} - ({this.TypeCadeau}) de {this.Proprietaire.Name}\n";
		}

	}
}
