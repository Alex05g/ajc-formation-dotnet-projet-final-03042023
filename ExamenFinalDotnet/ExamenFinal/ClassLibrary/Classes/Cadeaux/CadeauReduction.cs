﻿using ClassLibrary.Classes.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Classes.Cadeaux
{
	public class CadeauReduction : Cadeau
	{
		private int reduction;
		private string nomSite;
		public int Reduction { get => reduction; set => reduction = value; }
		public string NomSite { get => nomSite; set => nomSite = value; }

		public CadeauReduction(string nom_cadeau, Streamer proprietaire, string type_cadeau, int reduction, string nom_site)
		{
			this.NomCadeau = nom_cadeau;
			this.Proprietaire = proprietaire;
			this.TypeCadeau = type_cadeau;
			this.Reduction = reduction;
			this.NomSite = nom_site;
		}

		public override string RetournerInfosCadeau()
		{
			return $"\n{this.NomCadeau} - ({this.TypeCadeau}) de {this.Proprietaire.Name}\nRéduction de {this.Reduction} % sur le site {this.NomSite}\n";
		}
	}
}
