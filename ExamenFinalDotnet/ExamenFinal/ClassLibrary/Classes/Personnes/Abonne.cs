﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Classes.Personnes
{
	public class Abonne : Personne
	{
		private List<Abonnement> abonnements;
		public List<Abonnement> Abonnements { get => abonnements; set => abonnements = value; }

		public Abonne(string nom, string email)
		{
			this.Name = nom;
			this.Email = email;
			this.Type = "Abonné";
			this.Abonnements = new List<Abonnement>();
		}

		public override string RetournerInfosPersonne()
		{
			return base.RetournerInfosPersonne();
		}
	}
}
