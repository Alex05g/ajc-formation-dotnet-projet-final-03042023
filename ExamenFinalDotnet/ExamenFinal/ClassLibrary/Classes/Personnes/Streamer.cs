﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Classes.Cadeaux;

namespace ClassLibrary.Classes.Personnes
{
	public class Streamer : Personne
	{
		private Stream currentStream;
		private List<Abonne> abonnes;
		private List<Cadeau> cadeaux;
		private bool diffusion;

		public Stream CurrentStream { get => currentStream; set => currentStream = value; }
		public List<Abonne> Abonnes { get => abonnes; set => abonnes = value; }
		public List<Cadeau> Cadeaux { get => cadeaux; set => cadeaux = value; }
		public bool Diffusion { get => diffusion; set => diffusion = value; }

		public Streamer(string nom, string email)
		{
			this.Name = nom;
			this.Email = email;
			this.Abonnes = new List<Abonne>();
			this.Cadeaux = new List<Cadeau>();
			this.Diffusion = false;
		}

		public override string RetournerInfosPersonne()
		{
			return $"\nNom : {this.Name}\nEmail : {this.Email}\nRole : {this.Type}\nStatus actuel : {this.RetournerEtat()}\n";
		}

		public string RetournerEtat()
		{
			string etat;
			if (this.diffusion == true)
			{
				etat = "Est en live";
			}
			else
			{
				etat = "Est Hors ligne";
			}
			return etat;
		}

		public void OffrirCadeau(Personne person, Cadeau cadeau)
		{
			person.CadeauxRecus.Add(cadeau);
		}

		public void DemarrerStream(string nom_stream, Jeu jeu)
		{
			Stream stream = new Stream(nom_stream, jeu, this);
		}
	}
}
