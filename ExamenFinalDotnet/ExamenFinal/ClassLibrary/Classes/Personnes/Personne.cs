﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Classes.Cadeaux;

namespace ClassLibrary.Classes.Personnes
{
	public abstract class Personne
	{
		private int id;
		private string name;
		private string email;
		private string type;
		private List<Cadeau> cadeauxRecus;

		public int Id { get => id; set => id = value; }
		public string Name { get => name; set => name = value; }
		public string Email { get => email; set => email = value; }
		public string Type { get  => type; set => type = value; }
		public List<Cadeau> CadeauxRecus { get => cadeauxRecus; set => cadeauxRecus = value; }

		public virtual string RetournerInfosPersonne()
		{
			return $"\nNom : {this.Name}\nEmail : {this.Email}\nRole : {this.Type}\n";
		}
	}
}
