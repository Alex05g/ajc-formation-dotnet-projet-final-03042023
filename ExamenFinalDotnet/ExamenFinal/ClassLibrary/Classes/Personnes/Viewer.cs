﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Classes.Personnes
{
	public class Viewer : Personne
	{
		public Viewer(string nom, string email)
		{
			this.Name = nom;
			this.Email = email;
			this.Type = "Viewer";
		}

		public override string RetournerInfosPersonne()
		{
			return base.RetournerInfosPersonne();
		}
	}
}
