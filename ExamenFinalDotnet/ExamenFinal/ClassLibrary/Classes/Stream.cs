﻿using ClassLibrary.Classes.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Classes
{
	public class Stream
	{
		private int id;
		private string nomStream;
		private Jeu jeuDiffuse;
		private Streamer hote;
		private List<string> chat;
		private DateTime dateDebut;
		private int nombreSpectateurs;
		private List<Personne> spectateur;

		public int Id { get => id; set => id = value; }
		public string NomStream { get => nomStream; set => nomStream = value; }
		public Jeu JeuDiffuse { get => jeuDiffuse; set => jeuDiffuse = value; }
		public Streamer Hote { get => hote; set => hote = value; }
		public List<string> Chat { get => chat; set => chat = value; }
		public DateTime DateDebut { get => dateDebut; set => dateDebut = value; }
		public int NombreSpectateurs { get => nombreSpectateurs; set => nombreSpectateurs = value; }
		public List<Personne> Spectateur { get => spectateur; set => spectateur = value; }

		public Stream(string nom_stream, Jeu jeu_diffuse, Streamer hote_stream)
		{
			this.NomStream = nom_stream;
			this.JeuDiffuse = jeu_diffuse;
			this.Hote = hote_stream;
			this.Chat = new List<string>();
			this.DateDebut = DateTime.Now;
			this.NombreSpectateurs = 0;
			this.Spectateur = new List<Personne>();
		}
	}
}
