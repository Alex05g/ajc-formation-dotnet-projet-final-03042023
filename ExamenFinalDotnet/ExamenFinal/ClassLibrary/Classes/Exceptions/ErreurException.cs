﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Classes.Exceptions
{
	public class ErreurException : Exception
	{
		public ErreurException(string message) : base(message)
		{
		}
	}
}
