﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu.api
{
	public abstract class Displayer
	{
		Afficher afficher = Console.WriteLine;
		Lire lire = Console.ReadLine;

		public void AfficherSeparateur()
		{
            Console.WriteLine("\n_______________\n_______________\n");
        }

		public void AfficherConsole(string texte)
		{
			afficher(texte);
		}
		public string LireConsole()
		{
			string saisie = lire();
			return saisie;
		}

	}
}
