﻿using ClassLibrary.Classes;
using ClassLibrary.Classes.Cadeaux;
using ClassLibrary.Classes.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur.api
{
	public class GenerateurCadeaux
	{
		private Random randomTypeCadeau;
		private Random randomNombreCadeau;
		private int nombreCadeaux;
		private int nbTypeCadeau;
		public Random RandomTypeCadeau { get => randomTypeCadeau; set => randomTypeCadeau = value; }
		public int NbTypeCadeau { get => nbTypeCadeau; set => nbTypeCadeau = value; }
		public Random RandomNombreCadeau { get => randomNombreCadeau; set => randomNombreCadeau = value; }
		public int NombreCadeaux { get => nombreCadeaux; set => nombreCadeaux = value; }

		public GenerateurCadeaux()
		{
			this.RandomNombreCadeau = new Random();
			this.NombreCadeaux = RandomNombreCadeau.Next(5, 31);
		}

		public List<Cadeau> GenererListeCadeaux(Streamer streamer)
		{
			List<Cadeau> list_cadeaux = new List<Cadeau>();
			for (int i = 0; i < this.NombreCadeaux; i++)
			{
				this.RandomTypeCadeau = new Random();
				this.NbTypeCadeau = RandomNombreCadeau.Next(1, 5);
				if (NbTypeCadeau == 1)
				{
					Jeu jeu = new Jeu();
					jeu.NomJeu = jeu.GenererNomJeu();
					jeu.Plateforme = jeu.GenererNomPlateforme();
					CadeauOffert cadeauOffert = new CadeauOffert($"Un jeu offert #{GenererNomCadeau()}",streamer , "Jeu offert", $"{jeu.NomJeu}", $"{jeu.Plateforme}");
					list_cadeaux.Add(cadeauOffert);
				}
				else if (NbTypeCadeau == 2)
				{
					CadeauReduction cadeauReduction = new CadeauReduction($"Réduction #{GenererNomCadeau()}", streamer, "Reduction", 30, $"Site #{GenererNomCadeau}");
					list_cadeaux.Add(cadeauReduction);
				}
				else if (NbTypeCadeau == 3)
				{
					CadeauStreamer cadeauStreamer = new CadeauStreamer($"Session de jeu avec {streamer.Name} #{GenererNomCadeau()}", streamer, "Session avec streamer",streamer , 3);
					list_cadeaux.Add(cadeauStreamer);
				}
				else
				{
					Jeu jeu = new Jeu();
					jeu.NomJeu = jeu.GenererNomJeu();
					jeu.Plateforme = jeu.GenererNomPlateforme();
					CadeauTuto cadeauTuto = new CadeauTuto($"Tutoriel #{GenererNomCadeau()}", streamer, "Tutoriel", jeu.GenererNomJeu(), jeu.GenererNomPlateforme());
					list_cadeaux.Add(cadeauTuto);
				}
			}
			return list_cadeaux;
		}

		public string GenererNomCadeau()
		{
			Random random = new Random();
			int nb = random.Next(100, 999);
			string result = nb.ToString();
			return result;
		}

	}
}
