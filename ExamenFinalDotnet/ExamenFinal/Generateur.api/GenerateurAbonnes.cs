﻿using ClassLibrary.Classes.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur.api
{
	public class GenerateurAbonnes
	{
		private Random random;
		private Random nomRandom;
		private Random emailRandom;
		private int nbRandom;

		public Random Random { get => random; set => random = value; }
		public int NbRandom { get => nbRandom; set => nbRandom = value; }
		public Random NomRandom { get => nomRandom; set => nomRandom = value; }
		public Random EmailRandom { get => emailRandom; set => emailRandom = value; }

		public GenerateurAbonnes()
		{
			this.Random = new Random();
			this.NbRandom = Random.Next(1, 15);
		}

		public List<Abonne> GenererListeAbonnes()
		{
			List<Abonne> list_abonnes = new List<Abonne>();
			for (int i = 0; i < this.NbRandom; i++)
			{
				Abonne abonne = new Abonne($"abonné{NomRandom.Next(100, 999)}", $"{EmailRandom.Next(10, 99)}@gmail.com");
				list_abonnes.Add(abonne);
			}
			return list_abonnes;
		}
	}
}
