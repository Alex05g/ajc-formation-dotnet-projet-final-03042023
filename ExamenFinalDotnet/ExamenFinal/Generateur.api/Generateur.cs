﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generateur.api
{
	public abstract class Generateur
	{
		public string GenererUnPseudo()
		{
			List<string> list = new List<string>();
			list.Add("Panda");
			list.Add("campeur");
			list.Add("leDingue");
			list.Add("H4CKMAN");
			list.Add("alien");
			Random rdm = new Random();
			int nb_random = rdm.Next(1, 6);
			string debutNom = list.ElementAt(nb_random);
			int rdmNombre = GenererUnNombre();
			string nomTermine = $"{debutNom}{rdmNombre.ToString()}";
			return nomTermine;
		}

		public int GenererUnNombre()
		{
			Random rdm = new Random();
			int rdmNombre = rdm.Next(10, 51);
			return rdmNombre;
		}
	}
}
